from tkinter import *
import data_base_access
import camere
import winsound
import items
import category
from PIL import ImageTk, Image
import os

#global varibal
my_categor = category.categor()
my_item = items.item()
search_entry = None
data_base = data_base_access.dataBaseA("SqlStorage.db")

# Create object
root = Tk()

def main():
    """
    This function is the manage
    :return: None
    """
    set_background_picture()
    openScreen()
    # Execute tkinter
    root.mainloop()

def search_category():
    pass


def return_item_to_categor():
    destroy_param((Entry, Button))
    Entery_data(TRUE, "enter item name")
    submit_button = Button(root, text="Submit", bg="green", command=submit_returning_item)
    submit_button.pack(side=TOP, pady=10)

def submit_returning_item():
    global search_entry
    global my_item
    #get the text entered in the search entry
    search_text = search_entry.get()

    if(data_base.return_item_to_categor(search_text)):
        openScreen(search_text + " was return")
    else:
        openScreen(search_text + "was not taken")


def get_item():
    destroy_param((Entry, Button))
    Entery_data(TRUE, "enter item name")
    submit_button = Button(root, text="Submit", bg="green", command=submit_getting_item)
    submit_button.pack(side=TOP, pady=10)

def close_button():
    # Add buttons
#    close_button = Button(root, text="Close", command=close_window, bg="red",
 #                          width=100, height=10)
    image1 = Image.open("images\\cancel.png")
    resized_image = image1.resize((100, 100))

    # Convert image to PhotoImage
    photo_image = ImageTk.PhotoImage(resized_image)

    # Create a button with the image
    button = Button(root, image=photo_image, command=close_window)
    button.image = photo_image

    # Position the button3q 2qqqqqqqqq
    button.place(relx=0.95, rely=0.1, anchor=CENTER)

def submit_set_taken():
    global search_entry
    global my_item
    #get the text entered in the search entry
    search_text = search_entry.get()

    if len(search_text) > 1:
        user_id = data_base.get_user_id(search_text)
        if(user_id != -1 and my_item):
            data_base.set_take(user_id, my_item.get_id())
        openScreen(my_item.get_name() + " taken!")
    else:
        openScreen("this user name is invalid")

def submit_getting_items():
    global search_entry
    global my_item
    #get the text entered in the search entry
    search_text = search_entry.get()
    if len(search_text) > 1:
        return data_base.get_items_info(search_text)


def submit_getting_item():
    global search_entry
    global my_item
    #get the text entered in the search entry
    search_text = search_entry.get()
    if len(search_text) > 1:
        my_item = data_base.get_item_info(search_text)
        taken_user = data_base.item_taken_user(my_item)
        if(taken_user == -1):
            destroy_param((Entry, Button))
            Entery_data(TRUE, "enter user name")
            submit_button = Button(root, text="Submit", bg="green", command=submit_set_taken)
            submit_button.pack(side=TOP, pady=10)
        else:
            openScreen("this item was taken by: " + taken_user)
    else:
        openScreen("this item name is invalid")
def show_item():
    destroy_param((Entry, Button))
    Entery_data(TRUE, "enter item name")
    submit_button = Button(root, text="Submit", bg="green", command=entery_show_item)
    submit_button.pack(side=TOP, pady=10)

def entery_show_item():
    global search_entry
    global my_item  # Define the global my_item variable
    a = submit_getting_items()
    destroy_param((Listbox, Button, Entry))
    listb = Listbox(root)
    for item in a:
        listb.insert("end", item.get_name())
    listb.pack()

    select_button = Button(root, text="Select", command=lambda: selectItem(a[listb.curselection()[0]]))
    select_button.pack()

def selectItem(selected_item):
    global my_item  # Use the global my_item variable
    my_item = selected_item  # Set the global my_item variable
    if my_item:
        show_item_info(my_item.get_name(), my_item.get_path(), my_item.get_info(), my_item.get_category_id())

def entery_show_itemm():
    global search_entry
    global my_item
    submit_getting_item()
    show_item_info(my_item.get_name(), my_item.get_path(), my_item.get_info(), my_item.get_category_id())


def show_item_info(name, path, info, category_id):
    destroy_param((Entry, Button, Label, Listbox))

    # set background picture
    set_background_picture()

    label2 = Label(root, text= name + " data:"
                   ,width=10, height =3,
                   bg="yellow")
    #label2.pack(pady=10)

    message_name = Label(root, text="name: " + name
                   ,width=30, height =6,
                   bg="yellow")
    message_name.pack(anchor="center", expand=True)

    message_info = Label(root, text="info: " + info
                   ,width=30, height =6,
                   bg="yellow")
    message_info.pack(anchor="center", expand=True)

    message_category = Label(root, text="category: " + data_base.get_category_name_by_id(category_id)
                   ,width=30, height =6,
                   bg="yellow")
    message_category.pack(anchor="center", expand=True)

    #find name
    taken_name = data_base.item_taken_user(my_item)
    if(taken_name == -1):
        text = "not taking"
    else:
        text = "Taken name: " + taken_name
    message_take = Label(root, text=text
                   ,width=30, height =6,
                   bg="yellow")
    message_take.pack(anchor="center", expand=True)

    # Load image and resize it
    image1 = Image.open(path)
    resized_image = image1.resize((200, 200))

    # Convert image to PhotoImage
    photo_image = ImageTk.PhotoImage(resized_image)

    # Create a button with the image
    button = Button(root, image=photo_image, command=lambda: open_image(path))
    button.image = photo_image

    # Position the button
    button.place(relx=0.2, rely=0.2, anchor=CENTER)

    # Add buttons
    close_button = Button(root, text="Go back", command=openScreen, bg="blue",
                          width=20, height=5)
    close_button.pack(side="right", anchor="nw")

def open_image(path):
    os.startfile(path)

def adding_screen():
    destroy_param((Entry, Button, Label))
    set_background_picture()

    label2 = Label(root, text="Adding zone"
                   ,width=10, height =3,
                   bg="yellow")
    label2.pack(pady=10)

    button2 = Button(root, text="Add user", command=addUser
                     ,font=("Arial", 10),bg="blue"
                     ,width=20, height =5)
    button2.pack(side="left", anchor="nw")

    add_item_button = Button(root, text="add Item", command=addItem, bg="brown" ,font=("Arial", 10)
                                                     ,width=20, height =5)
    add_item_button.pack(side="left", anchor="nw")

    add_item_button = Button(root, text="add categor", command=add_category, bg="brown" ,font=("Arial", 10)
                                                     ,width=20, height =5)
    add_item_button.pack(side="left", anchor="nw")

    # Add buttons
    close_button = Button(root, text="Go back", command=openScreen, bg="blue",
                          width=20, height=5)
    close_button.pack(side="right", anchor="nw")


def openScreen(param="Welcome"):
    """
    This function will
    show the open screen
    :param param: message box
    :type str
    """
    destroy_param((Entry, Button, Label))

    # set background picture
    set_background_picture()

    label2 = Label(root, text="My storage"
                   ,width=10, height =3,
                   bg="yellow")
    label2.pack(pady=10)

    message = Label(root, text=param
                   ,width=30, height =6,
                   bg="yellow")
    message.pack(anchor="center", expand=True)

    # Add buttons
    close_button()

    adding_button = Button(root, text="to the adding zone", command=adding_screen, bg="blue" ,font=("Arial", 10)
                                                     ,width=20, height =5)
    adding_button.pack(side="left", anchor="nw")

    search_button = Button(root, text="to the searching zone", command=searching_scree, bg="brown" ,font=("Arial", 10)
                                                     ,width=20, height =5)
    search_button.pack(side="left", anchor="nw")

    take_button = Button(root, text="get item", command=get_item, bg="brown" ,font=("Arial", 10)
                                                     ,width=20, height =5)
    take_button.pack(side="left", anchor="nw")

    return_button = Button(root, text="return item", command=return_item_to_categor, bg="brown" ,font=("Arial", 10)
                                                     ,width=20, height =5)
    return_button.pack(side="left", anchor="nw")


def searching_scree():
    destroy_param((Label, Button))
    set_background_picture()

    label2 = Label(root, text="searching options"
                   ,width=15, height =3,
                   bg="yellow")
    label2.pack(pady=10)

    search_button = Button(root, text="Search users", command=find_user, bg="brown" ,font=("Arial", 10)
                                                     ,width=20, height =5)
    search_button.pack(side="left", anchor="nw")

    search_button_item = Button(root, text="Search item", command=show_item, bg="brown" ,font=("Arial", 10)
                                                     ,width=20, height =5)
    search_button_item.pack(side="left", anchor="nw")

        # Add buttons
    close_button = Button(root, text="Go back", command=openScreen, bg="blue",
                          width=20, height=5)
    close_button.pack(side="right", anchor="nw")


def addItem():
    global my_item
    Entery_data(TRUE, "enter name")
    submit_button = Button(root, text="Submit", bg="green", command=submit_add_item)
    submit_button.pack(side=TOP, pady=10)

def submit_add_info():
    global my_item
    """
    :return:
    """
    global search_entry
    # get the text entered in the search entry
    search_text = search_entry.get()

    # do something with the search text (e.g. print it)
    destroy_param((Entry, Button, Label))
    my_item.set_info(search_text)
    destroy_param((Entry, Button))
    Entery_data(TRUE, "enter category name")
    submit_button = Button(root, text="Submit", bg="green", command=submit_add_CATEGORY)
    submit_button.pack(side=TOP, pady=10)

def submit_add_CATEGORY():
    global my_item
    global search_entry
    # get the text entered in the search entry
    search_text = search_entry.get()
    if(data_base.get_categor_id(search_text) != -1):
        my_item.set_category_id(str(data_base.get_categor_id(search_text)))
        data_base.add_item(my_item)
        openScreen("item set")
    else:
        openScreen(search_text + " is not a name of category(item not add)")

def submit_add_item():
    global my_item
    """
    :return:
    """
    global search_entry
    # get the text entered in the search entry
    search_text = search_entry.get()

    # do something with the search text (e.g. print it)
    print(f"Search text: {search_text}")
    destroy_param((Entry, Button))
    my_item.set_name(str(search_text))
    message = Label(root, text="the name is: " + search_text + "now get picture of\n"
                                                               "the item"
               ,width=30, height =6,
               bg="yellow")
    message.pack(anchor="center", expand=True)
    mac = camere.CameraApp()
    mac.show_camera(search_text)
    my_item.set_path("items\\" + search_text + ".png")
    destroy_param((Entry, Button))
    Entery_data(TRUE, "enter info", 150)
    submit_button = Button(root, text="Submit", bg="green", command=submit_add_info)
    submit_button.pack(side=TOP, pady=10)


def Entery_data(isSend=FALSE, message="", width=10):
    global search_entry

    destroy_param((Button, Label))

    if(isSend):
        message = Label(root, text=message
               ,width=30, height =6,
               bg="yellow")
        message.pack(anchor="center", expand=True)

    set_background_picture()
    # add search option(entry)
    search_entry = Entry(root, width=width)
    search_entry.pack(side=TOP, pady=20)


def addUser():
    """
    This function will add
    user to the data base
    :return: None
    """
    Entery_data()
    # add submit button
    submit_button = Button(root, text="Submit", bg="green", command=submit_entry)
    submit_button.pack(side=TOP, pady=10)

def find_user():
    Entery_data()
    # add submit button
    submit_button = Button(root, text="Submit", bg="green", command=submit_find_user)
    submit_button.pack(side=TOP, pady=10)

def add_category():
    Entery_data(TRUE, "enter name")
    # add submit button
    submit_button = Button(root, text="Submit", bg="green", command=submit_find_category)
    submit_button.pack(side=TOP, pady=10)

def submit_find_category():
    global search_entry
    global my_categor
    # get the text entered in the search entry
    search_text = search_entry.get()
    if(data_base.get_categor_id(search_text) != -1):
        openScreen("this name was taken")
    else:
        destroy_param((Entry, Button, Label))
        Entery_data(TRUE, "enter info")
        # add submit button
        submit_button = Button(root, text="Submit", bg="green", command=submit_creat_categor)
        submit_button.pack(side=TOP, pady=10)
        my_categor.set_name(search_text)

def submit_creat_categor():
    global search_entry

    #get text
    search_text = search_entry.get()

    # do something with the search text (e.g. print it)
    print(f"Search text: {search_text}")
    destroy_param((Entry, Button, Label))
    my_categor.set_info(search_text)
    data_base.add_categor(my_categor)
    openScreen("creat categor")


def submit_find_user():
    global search_entry

    #get text
    search_text = search_entry.get()

    # do something with the search text (e.g. print it)
    print(f"Search text: {search_text}")
    destroy_param((Entry, Button))
    data = data_base.check_user_exists(search_text)
    if(data):
        openScreen(data)
    else:
        data =  data_base.user_likey_name(search_text)
        if(data):
            openScreen(("name didnt found,\n maybe you mean this - \n\n" + data))
        else:
            openScreen("name didnt found")

def destroy_param(param):
    # loop over all widgets in the window and destroy any buttons or labels
    for widget in root.winfo_children():
        if isinstance(widget, param):
            widget.destroy()


def submit_entry():
    global search_entry

    # get the text entered in the search entry
    search_text = search_entry.get()

    # do something with the search text (e.g. print it)
    print(f"Search text: {search_text}")
    destroy_param((Entry, Button))
    if(not data_base.check_user_exists(search_text)):
        data_base.add_user(search_text)
        openScreen("user added")
    else:
        openScreen("that name was taken")



def set_background_picture():
    # Load image file
    img = PhotoImage(file="images\\StorageBackground.png")

    # Create a label with the image and add it to the root window
    label = Label(root, image=img)
    label.pack(fill=BOTH, expand=YES)

    # Set the label as the background image and raise it to the back
    label.image = img
    label.place(x=0, y=0, relwidth=1, relheight=1)
    label.lower()

    # Adjust size
    root.geometry("1000x500")


def close_window():
    # Play a beep sound
    frequency = 1500  # Set frequency to 1500 Hz
    duration = 100  # Set duration to 100 milliseconds
    winsound.Beep(frequency, duration)

    data_base.close()
    root.destroy()


if __name__ == '__main__':
    main()
