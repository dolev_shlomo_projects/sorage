import cv2
from tkinter import *
import datetime
from tkinter import filedialog
import shutil
import os

class CameraApp:
    def __init__(self):
        self.window = Tk()
        self.window.title("Camera")

    def show_camera(self, name):
        cap = cv2.VideoCapture(0)
        #create button
        exit_button = Button(self.window, text='exit', width=14, bg='red', fg='black', command=self.window.destroy).place(relx=0.5, rely=0.3, anchor=CENTER)
        image_button = Button(self.window, text='take image', width=14, bg='green', fg='black', command=lambda: self.get_pic(name))
        image_button.place(relx=0.5, rely=0.7, anchor=CENTER)
        choos_file_button = Button(self.window, text="choose from files", width=14, bg='green', fg='black', command=lambda: self.get_file(name))
        choos_file_button.place(relx=0.5, rely=0.3, anchor=CENTER)
        # create a new Toplevel window for the image button
        image_button_window = Toplevel(self.window)
        image_button_window.title("Image Button Window")
        image_button_window.geometry("0x0")
        # wait for the image button to be pushed
        self.window.wait_window(image_button_window)

    def get_file(self, a_name):
        # Create a tkinter window
        root = Tk()
        root.withdraw()

        # Ask the user to select a file
        file_path = filedialog.askopenfilename()

        # Get the filename from the file path
        file_name = os.path.basename(file_path)

        # Copy the file to the current directory
        shutil.copy2(file_path, "items\\" + a_name + ".png")
        cv2.destroyAllWindows()
        # destroy the image button window to resume the execution of the show_camera() function
        self.window.destroy()

    def get_pic(self, a_name):
        """
        This function will get a picture and save
        :return: Null
        """
        cap = cv2.VideoCapture(0)  # open camera

        oclock_befor = datetime.datetime.now().time().second
        while True:
            #secondes_now = datetime.datetime.now().time().second
            _, frame = cap.read()

            cv2.imshow("My camera", frame)
            oclock_now = datetime.datetime.now().time().second
            distance = (oclock_now - oclock_befor) % 59
            if ((cv2.waitKey(1) == ord('q')) or distance == 3):
                cv2.imwrite("items\\" + "my_file.png", frame)
                shutil.copy2("items\\" + "my_file.png", "items\\" + a_name + ".png")
                break
        cap.release()
        cv2.destroyAllWindows()
        # destroy the image button window to resume the execution of the show_camera() function
        self.window.destroy()
