import sqlite3
import items
import category

class dataBaseA:
    def __init__(self, db_path):
        self.conn = sqlite3.connect(db_path)
        self.cursor = self.conn.cursor()
        self.create_tables()

    def item_taken_user(self, my_item):
        message = '''
        SELECT USERS.* FROM USERS
        JOIN TAKES ON TAKES.ID_USER = USERS.ID
        WHERE TAKES.ID_ITEM = ''' + str(my_item.get_id()) + '''
        '''
        self.cursor.execute(message)
        row = self.cursor.fetchone()
        if(row):
            return row[1]
        return -1

    def searching_name_string(self, name):
        return "'%" + '%'.join([tav for tav in name]) + "%'"

    def create_tables(self):
        #catefgory table
        self.cursor.execute('''CREATE TABLE IF NOT EXISTS CATEGORY
                   (ID INTEGER PRIMARY KEY NOT NULL, NAME TEXT NOT NULL, INFO TEXT NOT NULL)''')
        #items table
        self.cursor.execute('''CREATE TABLE IF NOT EXISTS ITEMS
                   (ID INTEGER PRIMARY KEY NOT NULL, NAME TEXT NOT NULL, INFO TEXT NOT NULL, PATH TEXT NOT NULL, ID_CATEGORY INTEGER NOT NULL,
                   FOREIGN KEY(ID_CATEGORY) REFERENCES CATEGORY(ID))''')
        #users table
        self.cursor.execute('''CREATE TABLE IF NOT EXISTS USERS
                   (ID INTEGER PRIMARY KEY NOT NULL, NAME TEXT NOT NULL)''')
        #takes table
        self.cursor.execute('''CREATE TABLE IF NOT EXISTS TAKES
                       (ID INTEGER PRIMARY KEY NOT NULL, 
                        ID_ITEM INTEGER NOT NULL, 
                        ID_USER INTEGER NOT NULL,
                        FOREIGN KEY(ID_ITEM) REFERENCES ITEMS(ID),
                        FOREIGN KEY(ID_USER) REFERENCES USERS(ID))''')


    def add_categor(self, categor):
        message = "INSERT INTO CATEGORY (NAME, INFO) VALUES('" + categor.get_name() + "', '" + categor.get_info() + "')"
        self.cursor.execute(message)
        self.conn.commit()

    def add_item(self, item):
        message = "INSERT INTO ITEMS (NAME, PATH, INFO, ID_CATEGORY) VALUES ('" + item.get_name() + "', '" + item.get_path() + "', '" + item.get_info() + "', " + str(item.get_category_id()) + ")"
        self.cursor.execute(message)
        self.conn.commit()
        # get the result of the INSERT statement
        result = self.cursor.fetchall()
        print(result)


    def get_categor_id(self, name):
        message = "SELECT * FROM CATEGORY WHERE NAME =  '" + name + "'"
        self.cursor.execute(message)
        row = self.cursor.fetchone()
        if(row):
            return row[0]
        return -1


    def get_items_from_categor(self, categor_name):
        my_items = []
        pass

    def get_item_info(self, name):
        my_item = items.item()
        message = "SELECT * FROM ITEMS WHERE NAME =  '" + name +"'"
        self.cursor.execute(message)
        row = self.cursor.fetchone()

        #if not find look for like the name
        if(not row):
            message = "SELECT * FROM ITEMS WHERE NAME LIKE  '"
            for tav in name:
                message += "%" + tav
            message += "%'"
            self.cursor.execute(message)
            row = self.cursor.fetchone()
        #if row exists
        if(row):
            my_item.set_id(row[0])
            print(my_item.get_id())
            my_item.set_name(row[1])
            my_item.set_info(row[2])
            my_item.set_category_id(row[3])
            my_item.set_path(row[4])
            return my_item
        return 0

    def get_items_info(self, name):
        items_info = []

        message = "SELECT * FROM ITEMS WHERE NAME = '" + name + "'"
        self.cursor.execute(message)
        rows = self.cursor.fetchall()

        # If not found, look for names containing the provided name
        if not rows:
            message = "SELECT * FROM ITEMS WHERE NAME LIKE '"
            for tav in name:
                message += "%" + tav
            message += "%'"
            self.cursor.execute(message)
            rows = self.cursor.fetchall()

        # Process rows and populate items_info list
        for row in rows:
            my_item = items.item()
            my_item.set_id(row[0])
            my_item.set_name(row[1])
            my_item.set_info(row[2])
            my_item.set_category_id(row[3])
            my_item.set_path(row[4])
            items_info.append(my_item)

        return items_info


    def get_category_name_by_id(self, id):
        message = "SELECT * FROM CATEGORY WHERE ID =  " + str(id) + ""
        self.cursor.execute(message)
        row = self.cursor.fetchone()
        if(row):
            return row[1]
        return -1


    def get_item_id(self, name):
        my_item = items.item()
        message = "SELECT * FROM ITEMS WHERE NAME =  '" + name + "'"
        self.cursor.execute(message)
        row = self.cursor.fetchone()
        if(row):
            return row[0]
        return -1


    def add_user(self, name):
        self.cursor.execute("INSERT INTO USERS (NAME) VALUES('" + name + "')")
        self.conn.commit()

    def set_take(self, id_user, id_item):
        self.cursor.execute("INSERT INTO TAKES (ID_ITEM, ID_USER) VALUES(" + str(id_item) + ", " + str(id_user) + ")")
        self.conn.commit()

    def check_item_take(self, name):
        message = "SELECT * FROM ITEMS         " \
                  "JOIN TAKES ON TAKES.ID_ITEM = ITEMS.ID " \
                  "WHERE ITEMS.NAME LIKE '"
        for tav in name:
            message += "%" + tav
        message += "%'"
        self.cursor.execute(message)
        self.conn.commit()
        return self.cursor.fetchone()

    def return_item_to_categor(self, name):
        item = self.get_item_info(name)
        if(item and self.check_item_take(name) and name):
            message = '''DELETE FROM TAKES 
            WHERE ID_ITEM IN (
                SELECT ID FROM ITEMS 
                WHERE NAME LIKE ''' + self.searching_name_string(name) + ''')'''
            self.cursor.execute(message)
            self.conn.commit()
            return True
        else:
            return False

    def get_user_id(self, name):
        self.cursor.execute("SELECT * FROM USERS WHERE NAME = '" + name + "' ")
        row = self.cursor.fetchone()
        if(row):
            return row[0]
        return -1

    def check_user_exists(self, name):
        self.cursor.execute("SELECT * FROM USERS WHERE NAME = '" + name + "' ")
        row = self.cursor.fetchone()
        result = ''
        if(row):
            result = "ID: {}, Name: {}\n".format(row[0], row[1])
        return result

    def user_likey_name(self, name):
        message = "SELECT * FROM USERS WHERE NAME LIKE '"
        for tav in name:
            message += "%" + tav
        message += "%'"
        self.cursor.execute(message)
        rows = self.cursor.fetchall()
        result = ''
        for row in rows:
            result += 'ID: {}, Name: {}\n'.format(row[0], row[1])
        return result

    def close(self):
        self.conn.close()
