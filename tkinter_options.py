import cv2
import numpy as np


# Load the image
img = cv2.imread('my_file.png')
# Convert to grayscale
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

# Apply thresholding to separate the foreground from the background
thresh = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]

# Invert the foreground mask to get the background mask
background_mask = cv2.bitwise_not(thresh)

# Apply the background mask to the original image
background = cv2.bitwise_and(img, img, mask=background_mask)

# Display the result
cv2.imshow('Background', background)
cv2.waitKey(0)
cv2.destroyAllWindows()
