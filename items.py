
class item:
    def __init__(self):
        pass

    def set_id(self, id):
        self.id = id

    def get_id(self):
        return self.id

    def set_name(self, name):
        self.name = name

    def get_name(self):
        return self.name

    def set_path(self, path):
        self.path = path

    def get_path(self):
        return self.path

    def set_info(self, info):
        self.info = info

    def get_info(self):
        return self.info

    def set_category_id(self, id):
        self.category_id = id

    def get_category_id(self):
        return self.category_id
